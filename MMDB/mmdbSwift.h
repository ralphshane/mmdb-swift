//
//  mmdbSwift.h
//  MMDB
//
//  Created by Ralph Shane on 25/11/2016.
//  Copyright © 2016 LexTang.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMDBContinent : NSObject
@property(nonatomic, strong) NSString * _Nullable code;
@property(nonatomic, strong) NSDictionary<NSString *, NSString *> * _Nullable names;
@end

@interface MMDBCountry : NSObject
@property(nonatomic, strong) MMDBContinent * _Nonnull continent;
@property(nonatomic, strong) NSString * _Nonnull isoCode;
@property(nonatomic, strong) NSDictionary<NSString *, NSString *> * _Nonnull names;
- (_Nullable instancetype) initWithDictionary:(NSDictionary * _Nullable)dictionary;
@end

@interface MMDB : NSObject
- (_Nullable  instancetype) initWithFileName:(NSString * _Nullable)fileName;
- (MMDBCountry * _Nullable) lookup:(NSString * _Nonnull)IPString;
@end
