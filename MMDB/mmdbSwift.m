//
//  mmdbSwift.m
//  MMDB
//
//  Created by Ralph Shane on 25/11/2016.
//  Copyright © 2016 LexTang.com. All rights reserved.
//

#import "mmdbSwift.h"

#import "maxminddb.h"
#import "maxminddb-compat-util.h"
#import "maxminddb_unions.h"


@implementation MMDBContinent
@end


@implementation MMDBCountry

- (instancetype) initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        _continent = [[MMDBContinent alloc] init];
        _isoCode = @"";
        _names = [NSDictionary dictionary];

        NSDictionary *dict = dictionary[@"continent"];
        NSString *code = dict[@"code"];
        NSDictionary<NSString *, NSString *> *continentNames = dict[@"names"];
        if (dict && code && continentNames) {
            _continent.code = code;
            _continent.names = continentNames;
        }

        dict = dictionary[@"country"];
        NSString *iso = dict[@"iso_code"];
        NSDictionary<NSString *, NSString *> *countryNames = dict[@"names"];
        if (dict && iso && countryNames) {
            _isoCode = iso;
            _names = countryNames;
        }
    }
    return self;
}

- (NSString *) description {
    NSMutableString *s = [NSMutableString string];
    [s appendString:@"{\n"];
    [s appendString:@"  \"continent\": {\n"];
    [s appendFormat:@"    \"code\": \" %@ \",\n", self.continent.code];
    [s appendString:@"    \"names\": {\n"];

    __block NSUInteger i = self.continent.names.count;
    [self.continent.names enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        [s appendString:@"      \""];
        [s appendFormat:@"%@: \"", key];
        [s appendFormat:@"%@\"", obj];
        [s appendString:((i > 1) ? @"," : @"")];
        [s appendString:@"\n"];
        i -= 1;
    }];

    [s appendString:@"    }\n"];
    [s appendString:@"  },\n"];
    [s appendFormat:@"  \"isoCode\": \"%@\",\n", self.isoCode];
    [s appendString:@"  \"names\": {\n"];
    i = self.names.count;
    [self.names enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        [s appendString:@"    \""];
        [s appendFormat:@"%@: \"", key];
        [s appendFormat:@"%@\"", obj];
        [s appendString:((i > 1) ? @"," : @"")];
        [s appendString:@"\n"];
        i -= 1;
    }];

    [s appendString:@"  }\n}"];
    return s;
}

@end


@implementation MMDB {
    MMDB_s _db;
}

- (instancetype) init {
    return [self initWithFileName:@""];
}

- (instancetype) initWithFileName:(NSString * _Nullable)fileName {
    if (self = [super init]) {
        if (fileName == nil) {
            fileName = @"";
        }
        const char *cfilename = fileName.UTF8String;
        int status = MMDB_open(cfilename, MMDB_MODE_MASK, &_db);
        if (status != MMDB_SUCCESS) {
            NSLog(@"%s", MMDB_strerror(errno));

            // Failover to db in bundle
            NSBundle *bundle = [NSBundle bundleForClass:[self class]];
            NSString *path = [bundle pathForResource:@"GeoLite2-Country" ofType:@"mmdb"];
            if (path == nil) {
                return nil;
            }
            cfilename = path.UTF8String;
            status = MMDB_open(cfilename, MMDB_MODE_MASK, &_db);
            if (status != MMDB_SUCCESS) {
                NSLog(@"%s", MMDB_strerror(errno));
                return nil;
            }
        }
    }
    return self;
}

- (BOOL) lookupString:(NSString *)s result:(MMDB_lookup_result_s *)result {
    const char *string = s.UTF8String;
    int gaiError = 0;
    int error = 0;

    MMDB_lookup_result_s _result __unused = MMDB_lookup_string(&_db, string, &gaiError, &error);
    if ((gaiError == noErr) && (error == noErr)) {
        // NSAssert(_result.found_entry, @"result.found_entry");
        if (result) {
            *result = _result;
        }
        return YES;
    }
    return NO;
}

- (NSString *) getString:(MMDB_entry_data_list_s *)list {
    MMDB_entry_data_s *data = &(list->entry_data);
    UInt32 type = data->type;
    // Ignore other useless keys
    if (data->has_data==false || type!=MMDB_DATA_TYPE_UTF8_STRING) {
        return @"";
    }
    const char *str = MMDB_get_entry_data_char(data);
    NSUInteger size = data->data_size;
    char *cKey = mmdb_strndup(str, size);
    NSString *key = [NSString stringWithUTF8String:cKey];
    free(cKey);

    return key;
}

- (UInt32) getType:(MMDB_entry_data_list_s *)list {
    MMDB_entry_data_s *data = &(list->entry_data);
    return (UInt32) data->type;
}

- (UInt32) getSize:(MMDB_entry_data_list_s *)list {
    return (UInt32)(list->entry_data.data_size);
}

- (MMDB_entry_data_list_s *) dumpList:(MMDB_entry_data_list_s *)inList toS:(NSMutableString **)toS {
    MMDB_entry_data_list_s *list = inList;
    switch ([self getType:list]) {
        case MMDB_DATA_TYPE_MAP:
        {
            [(*toS) appendString:@"{\n"];
            UInt32 size = [self getSize:list];
            list = list->next;
            while (size != 0 && list != NULL) {
                [(*toS) appendFormat:@"\"%@\":", [self getString:list]];
                list = list->next;
                list = [self dumpList:list toS:toS];
                size -= 1;
            }
            [(*toS) appendString:@"},"];
        }
            break;
        case MMDB_DATA_TYPE_UTF8_STRING:
            [(*toS) appendFormat:@"\"%@\",", [self getString:list]];
            list = list->next;
            break;
        case MMDB_DATA_TYPE_UINT32:
            [(*toS) appendFormat:@"%u,", *MMDB_get_entry_data_uint32(&(list->entry_data))];
            list = list->next;
            break;

        default:
            break;
    }
    return list;
}

- (NSString *) lookupJSON:(NSString *)s {
    MMDB_lookup_result_s result;
    if([self lookupString:s result:&result] == NO) {
        return nil;
    }
    MMDB_entry_s *entry = &result.entry;
    MMDB_entry_data_list_s *list = nil;
    int status = MMDB_get_entry_data_list(entry, &list);
    if (status != MMDB_SUCCESS) {
        return  nil;
    }

    NSMutableString *JSONString = [NSMutableString string];
    [self dumpList:list toS:&JSONString];

    MMDB_free_entry_data_list(list);

    return [JSONString stringByReplacingOccurrencesOfString:@"},},}," withString:@"}}}"];
}

- (MMDBCountry * _Nullable) lookup:(NSString *)IPString {
    NSString *s = [self lookupJSON:IPString];
    if (s == nil) {
        return nil;
    }
    NSData *data = [s dataUsingEncoding:NSUTF8StringEncoding];
    if (data == nil) {
        return nil;
    }
    @try {
        NSError *error = nil;
        NSDictionary *JSON =
        [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        if ([JSON isKindOfClass:[NSDictionary class]] == NO) {
            return nil;
        }
        return [[MMDBCountry alloc] initWithDictionary:JSON];
    } @catch (NSException *exception) {
        return nil;
    }
}

- (void) dealloc {
    MMDB_close(&_db);
}

@end
